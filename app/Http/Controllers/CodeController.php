<?php

namespace App\Http\Controllers;

use App\Models\Code;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CodeController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'code' => 'required',
        ]);

        $expiration = Carbon::now();
        switch ($request->expiration) {
            case '10 min':
                $expiration->addMinutes(10);
                break;
            case '1 hour':
                $expiration->addHour();
                break;
            case '3 hours':
                $expiration->addHours(3);
                break;
            case '1 day':
                $expiration->addDay();
                break;
            case '1 week':
                $expiration->addWeek();
                break;
            case '1 month':
                $expiration->addMonth();
                break;
            case 'no limit':
                $expiration = null;
                break;
        }

        $newCode = new Code();
            $newCode->name = $request->name;
            $newCode->code = $request->code;
            $newCode->hash = sha1(time());
            $newCode->expiration = $request->expiration;
            $newCode->expiration_date = $expiration ? $expiration->format('Y-m-d H:i:s') : null;
            $newCode->status = $request->status;
            $newCode->user_id = Auth::check() ? Auth::user()->id : 1;
        $newCode->save();

        return redirect(route('code.show', ['hash'=>$newCode->hash]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Code  $code
     * @return \Illuminate\Http\Response
     */
    public function show($hash)
    {
        $code = Code::where('hash', $hash)->first();
        $latest = Code::latest();
        abort_if(!$hash || !$code->availabe, 404);
        return view('code.show')
            ->with('code', $code)
            ->with('latest', $latest);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Code  $code
     * @return \Illuminate\Http\Response
     */
    public function edit($hash)
    {
        $code = Code::where('hash', $hash)->first();
        abort_if(!$code, 404);
        return view('code.edit', $code);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Code  $code
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $hash)
    {
        $code = Code::where('hash', $hash)->first();
        abort_if(!$code, 404);
            $code->name = $request->name;
            $code->code = $request->code;
            $code->hash = sha1(time());
            $code->expiration = $request->expiration;
            $code->status = $request->status;
            $code->user_id = Auth::check() ? Auth::user()->id : 1;
        $code->save();

        return redirect(route('home'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Code  $code
     * @return \Illuminate\Http\Response
     */
    public function destroy($hash)
    {
        $code = Code::where('hash', $hash)->first();
        abort_if(!$code, 404);
        $code->delete();
        return redirect(route('home'));
    }
}
