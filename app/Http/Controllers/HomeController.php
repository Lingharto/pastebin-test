<?php

namespace App\Http\Controllers;

use App\Models\Code;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $latest = Code::latest();
        return view('home')
            ->with('latest', $latest);
    }
}
