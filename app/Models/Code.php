<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Code extends Model
{
    protected $appends = array('availabe');

    public function getAvailabeAttribute()
    {
        if($this->expiration == 'no limit')
        {
            return true;
        }
        else
        {
            $expiration = Carbon::createFromFormat('Y-m-d H:i:s', $this->expiration_date);

            if($expiration->gte(Carbon::now())){
                return true;
            } else {
                return false;
            }
        }
    }

    public static function latest()
    {
        return Code::where('status', 'public')->where('expiration_date', '>=', Carbon::now())->orWhere('expiration_date', null)->orderBy('created_at')->take(10)->get();
    }
}
