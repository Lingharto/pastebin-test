<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/{hash}', 'CodeController@show')->name('code.show');
Route::post('/code/create', 'CodeController@store')->name('code.store');
Route::post('/code/edit/{hash}', 'CodeController@update')->name('code.update');
Route::get('/code/edit/{hash}', 'CodeController@edit')->name('code.edit');
Route::get('/code/destroy/{hash}', 'CodeController@destroy')->name('code.destroy');

