<div class="col-md-4">
    <div class="card">
        <div class="card-body">
            @if($latest)
                <ul>
                    @foreach($latest as $element)
                        <li><a href="{{route('code.show', ['hash'=>$element->hash])}}">{{$element->name}}</a></li>
                    @endforeach
                </ul>
            @else
                No snippets to display
            @endif
        </div>
    </div>
</div>