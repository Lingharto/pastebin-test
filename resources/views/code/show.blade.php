@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">

            @include('code.partials.latest')

            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Show code snippet <a href="{{route('code.show', ['hash'=>$code->hash])}}">{{$code->hash}}</a></div>

                    <div class="card-body">

                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div>
                            {{$code->code}}
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
