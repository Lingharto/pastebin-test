@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">

        @include('code.partials.latest')

        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Pastebin</div>

                <div class="card-body">

                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger" role="alert">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                        <form method="post" action="/code/create">

                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="expiration">Expiration time</label>
                                <select name="expiration" id="expiration" class="form-control">
                                    <option value="10 min">10 min</option>
                                    <option value="1 hour">1 hour</option>
                                    <option value="3 hours">3 hours</option>
                                    <option value="1 day">1 day</option>
                                    <option value="1 week">1 week</option>
                                    <option value="1 month">1 month</option>
                                    <option value="no limit">no limit</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="status">Status</label>
                                <select name="status" id="status" class="form-control">
                                    <option value="public">public</option>
                                    <option value="unlisted">unlisted</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" id="name">
                            </div>

                            <div class="form-group">
                                <label for="code">Code</label>
                                <textarea name="code" id="code" class="form-control code-edit"></textarea>
                            </div>

                            <button type="submit" class="btn btn-primary">Submit</button>

                        </form>

                </div>
            </div>
        </div>
    </div>
</div>


@endsection
