#!/usr/bin/env bash
sudo chmod -R 777 *
cat /dev/null > ./storage/logs/laravel.log
php artisan clear-compiled
php artisan cache:clear
composer dump-autoload
php artisan migrate:fresh --seed
